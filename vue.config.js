// 版本号
const Moment = require('moment');
const path = require('path');
// 生产环境删除console.log
// const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// 站点配置
const SITE_CONFIG = require('./src/config/project');
//path.join(__dirname)设置绝对路径
function resolve(dir){
    return path.join(__dirname,dir)
}

// 版本号
const pack = require('./package.json');
process.env.VUE_APP_VERSION = JSON.stringify(`${pack.version} ${Moment(new Date()).format( 'YYYY-MM-DD HH:mm:sss')}`);

console.log(SITE_CONFIG[process.env.VUE_APP_KEY],process.env.VUE_APP_VERSION);

const webpack = require("webpack");
module.exports = {
    outputDir: `./dist/${SITE_CONFIG[process.env.VUE_APP_KEY].DIR}/${SITE_CONFIG[process.env.VUE_APP_KEY].FILE_NAME}`,
    assetsDir: 'assets',
    indexPath: "index.html",
    filenameHashing: true,
    productionSourceMap: false,
    devServer: {
        port: 8888
    },
    chainWebpack: config => {
        config.resolve.symlinks(true);
        config.resolve.alias
        .set('@',resolve('./src'))
        .set('@components',resolve('./src/components'))
        .set('@views',resolve('src/views'))
        .set('@assets',resolve('src/assets'))  
        .set('@common',resolve('src/common'))  
        .set('@utils',resolve('src/utils'))
        .set('@config',resolve('src/config'));
    
        config.plugin("ignore").use(new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /zh-cn$/));
        return config;
    },

    configureWebpack: config => {
        config.plugins.push(
            new HtmlWebpackPlugin({
            //   favicon: path.resolve(__dirname, `./src/assets/favicon/${SITE_CONFIG[process.env.VUE_APP_KEY].FILE_NAME}/favicon.ico`),
              template: path.resolve(__dirname, './public/index.html'),
              minify: { // 压缩HTML文件
                minifyCSS: true// 压缩内联css
              },
              title: SITE_CONFIG[process.env.VUE_APP_KEY].PROJECT_NAME
            })
        )
    },

    lintOnSave: false
}