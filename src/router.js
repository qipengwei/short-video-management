import Vue from "vue";
import VueRouter from "vue-router";
import Vuex from './store';
import { uncheckRouters } from '@config/asyncRouters';
import 'nprogress/nprogress.css';
import { Message } from 'element-ui';
import NProgress from 'nprogress';
Vue.use(VueRouter);

//路由白名单
let WHITE_LIST = Vuex.getters.getSystemGui;
//非权限路由
let routes = [...uncheckRouters];

const createRouter = () => new VueRouter({ routes });

const router = createRouter();
//重置路由
const resetRouter = (list) => {
    router.matcher = createRouter().matcher;
    router.addRoutes(list);
};

// 监听
router.beforeEach((to, from, next) => {
    NProgress.start();
    if(to.meta.title) {
        document.title = `小苹果-${to.meta.title}`;
        next();
    }
    //校检是否登录
    let token = Vuex.getters.getToken;
    if (token) {
        //有token 如果是登录页面 则重定向到首页
        if (to.path === "/Login") {
            next("/");
            NProgress.done();
        } else {
            // 有token 不是登录页面 动态获取路由
            let roles = Vuex.getters.getUserPermissions;
            //没有权限表重新请求
            if (roles.length === 0) {
               Vuex.dispatch('generateNavConfig')
                   .then(res => {
                       //匹配路由
                       if (res !== null && Array.isArray(res) && res.length > 0) {
                           resetRouter(res);
                           next({ ...to, replace: true });
                       } else {
                           router.replace('/Login').then(r => console.log(r));
                           Message.error("您当前账号未分配权限！，请联系管理员");
                       }
                   })
                   .catch(err => {
                       //请求发生异常时 重定向回首页
                       router.replace('/Login').then(r => console.log(r));
                       Message.error("权限获取异常,请尝试重新登录");
                   })
            } else {
               //有权限表 放行
               next();
            }
        }
    } else {
        //没有token 并且跳转的页面在白名单中 则不需要进行任何校验
        if (WHITE_LIST.includes(to.path)) {
            next();
            NProgress.done();
        } else {
            // 如果跳转的页面不存在白名单中,则重定向到登录页面
            next('/Login');
            NProgress.done();
        }
    }

});

router.afterEach(() => NProgress.done());
export default router;
