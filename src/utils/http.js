
import axios from 'axios';
import Utils from '@utils/index';
import Vuex from '../store';
import VueRouter from "vue-router";
import { Message, Loading } from 'element-ui';
const PROJECT_CONFIG = require('@config/project');
const { URL } = PROJECT_CONFIG[process.env.VUE_APP_KEY];
import ENCRYPT_KEY from '@config/dataSource';

//loading对象
let loading;
//当前正在请求的数量
let needLoadingRequestCount = 0;
//显示loading
let showLoading = target => {
  if (needLoadingRequestCount === 0 && !loading) {
        loading = Loading.service({
        lock: true,
        text: "Loading...",
        background: 'rgba(255, 255, 255, 0.5)',
        target: target || "body"
        });
  }
  needLoadingRequestCount++;
};

//隐藏loading
let hideLoading = () => {
  needLoadingRequestCount--;
  needLoadingRequestCount = Math.max(needLoadingRequestCount, 0); //做个保护
  if (needLoadingRequestCount === 0) {
    //关闭loading
    toHideLoading();
  }
};

// 关闭loading
let toHideLoading = () => { 
    if(loading) {
        loading.close();
        loading = null;
    }
};

// 在发送请求之前做些什么
axios.interceptors.request.use(config => {
    //token
    if(Vuex.getters.getToken) {
        config.headers.authorization = Vuex.getters.getToken;
    }
    if(config.closeLoad !== false){
        showLoading(config.loadDom);
    }
    return config;
},error => {
    // 对请求错误做些什么
    Message.error('请求超时!');
    hideLoading();
    return Promise.reject(error);
})

axios.interceptors.response.use(response => {
    if(response.data.msg === "token expired") {
        Message.error('token授权过期!,请重新登陆');  
        Vuex.commit('resetUser');
        VueRouter.replace('/login');
    } else if (response.data.msg === "operation without permission") {
        //此处异步更新权限有待优化
        setTimeout(() => location.reload(), 0);
    } else if(response.data.code === 402) {
        Message.error('您的账号已在别处登录!');
        Vuex.commit('resetUser');
        VueRouter.replace('/login');
    }
    if(response.config.closeLoad !== false){
        hideLoading();
    }
    return response;
}, error => {
    // 对响应错误做点什么
    if(error.response && error.response.data && error.response.data.message) {
        let jsonObj = JSON.parse(error.response.data.message);
        Message.error(jsonObj.message);
        hideLoading();
    } else {
        if(error.message === 'Network Error') {
            Message.error('网络出了点问题~');
        } else {
            Message.error(error.message);
        }
        hideLoading();
    };
    return Promise.reject(error);
})

/**
 * 
 * @param {Object} option 业务参数配置
 * @param {Boolean} Load 请求loading开关
 * @param {String} loadDom 动画元素作用id 默认body
 */
export function axiosApi(option, Load = true, loadDom) {
    //时间戳
    let timestamp = (Utils.second()).toString();
    //随机字符串
    let nonce = Utils.rndHexString(7);
    //业务参数          
    let content = JSON.stringify(option.content);
    //签名
    let sign = Utils.encryption(content + timestamp + nonce).toString();
    //随机码加密 
    nonce = Utils.encryptCBC(nonce, ENCRYPT_KEY.AES_KEY, ENCRYPT_KEY.IV);
    //content加密
    content = Utils.encryptCBC(content, ENCRYPT_KEY.AES_KEY, ENCRYPT_KEY.IV);

    //格式化参数
    let httpDefault = { 
        method: option.way,
        url: `${URL}${option.method}`, 
        timeout: 300000,
        closeLoad: Load,
        loadDom 
    };

    let RES_MODE = '';
    if(['DELETE','GET'].includes(option.way)) {
        RES_MODE = 'params';
    } else if(['POST','PUT'].includes(option.way)){
        RES_MODE = 'data';
    }

    httpDefault[RES_MODE] = { content, timestamp, sign, nonce };
    //返回结果
    return new Promise((resolve, reject) => axios(httpDefault)
        .then(res => resolve(res))
           .catch(res => reject(res)));
}



