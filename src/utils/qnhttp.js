import * as qiniu from 'qiniu-js'
export function qnApi (file, token, obj) {
    let config = {
        useCdnDomain: true,
        disableStatisticsReport: false,
        retryCount: 6,
        region: qiniu.region.z0
    };
    let putExtra = {
        fname: "",
        params: {},
        mimeType: null
    };
    let observer = {   //上传时的监听函数
        next(res){   //上传进度
          if (obj.next) {
            obj.next(res)
          }
        },
        error(err){  //上传失败
          if (obj.error) {
              
            obj.error(err)
          }
        }, 
        complete(res){  //上传完成
          if (obj.complete) {
            obj.complete(res)
          }
        }
    }
    let observable = qiniu.upload(file, null, token, putExtra, config)  //配置  文件，key  token  上面配置信息
    observable.subscribe(observer) // 上传开始
    //subscription.unsubscribe() // 上传取消  subscription是上传开始赋值的变量
}

