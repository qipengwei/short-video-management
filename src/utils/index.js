
const Moment = require('moment');
const cryptoJs = require('crypto-js');
const Numeral = require('numeral');
const NP = require('number-precision');
class Utils {
   /**
     * 筛选导出集合字段
     * @param {Array} filterVal key数据集
     * @param {Array} jsonData 数据集
     */
   static formatJson(filterVal, jsonData) {
       return jsonData.map(v => filterVal.map(f => v[f]))
   }

    /**
     * 区间查询格式转换
     * @param {Array} portion 区间
     * @param {Boolean} isPriceQuery 是否金额查询
     */
   static formatPortion(portion, isPriceQuery = false) {
        if (Array.isArray(portion)) {
            if (portion.some(query => query != "")) {
                let medium = portion.slice();
                // if (isPriceQuery) {
                //     for (let i in medium) {
                //         medium[i] = !this.isStringNull(medium[i])
                //             ? this.computational('times', medium[i], 100)
                //             : "";
                //     };
                // }
                return medium.join(",");
            } else {
                return "";
            }
        } else {
            return "";
        }
   }

    /**
     * 截取文件名
     * @param {String} filename 
     */
    static extname(filename){
        if(!filename||typeof filename!='string'){
            return false;
        };
        let a = filename.split('').reverse().join('');
        let b = a.substring(0,a.search(/\./)).split('').reverse().join('');
        return b;
    }

   /**
   * 判断输入是否为整数
   * @param {Number} num 
   */
   static isInteger(num) {
        if(/^[1-9]\d*$/.test(num)){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 判断输入价格是否有效
     * @param {*} price 
     */
    static priceReg(price) {
        if(/(^[1-9]\d*(\.\d{1,2})?$)|(^0(\.\d{1,2})?$)/.test(price)){
            return true
        } else {
            return false
        }
    }

    /**
     * 判断num是否为一个有效的数字
     * @param {Number} num 
     */
    static validNum(num) {
        return typeof num == 'number' && isNaN(num);
    }

    /**
     * 筛选元素
     * @param {Array} collection 集合对象数组
     * @param {String} key 筛选key 
     * @param {String} sign 筛选标志
     * @param {String} way 筛选方法
     */
    static filterElement(collection,key,sign,way = 'find') {
        return collection[way](each => each[key] === sign);
    }

    static videoStatus(key) {
        let keys = {
            "0": {
                color: "text-warning-1"
            },
            "1": {
                color: "text-main-17"
            },
            "2": {
                color: "text-main-6"
            },
            "3": {
                color: "text-nav-gray"
            },
            "4": {
                color: "text-main-27"
            },
            "5": {
                color: "text-hint-1"
            },
            "6": {
                color: "text-main-26"
            }
        }

        return keys[key];
    }

    /**
     * 截取七牛文件资源hash
     * @param {String} url 文件url
     */
    static getHash(url){
        if(url.indexOf('/')!=-1){
            return url.split('/')[3];
        }else{
            return url
        }
    }
    /**
     * 
     * @param {String} type 截取类型
     * @param {String} sign 截取标志
     * @param {String} text 截取内容
     */
    static contentSplice(type,sign,text) {
        if(type == 1) {
            return text.indexOf(sign) != -1 ? text.substring(0,text.indexOf(sign)) : text;
        } else {
            return text.lastIndexOf(sign) != -1 ? text.substring(text.lastIndexOf(sign) + 1) : text;
        }
    }
    /**
     * 字符串去除空格
     * @param {String} str 
     */
    static isBlank(str) {
        let medium = typeof str != 'string' ? String(str) : str;
        return medium.replace(/\s/g,"");
    }
    /**
     * 金额格式化
     * @param {Number || String} price 金额
     * @param {String} format 格式 
     */
    static priceFormat(price,format = '0,0.00') {
        return Numeral(price).format(format);
    }
    /**
     * 两位计算
     * @param {String} key 方法名 {plus 加} {minus 减} {times 乘} {divide 除}
     * @param {Number} num1 
     * @param {Number} num2 
     */
    static computational(key,num1,num2) {
        return NP[key](num1,num2);
    }

    /**
     * 字符串 / 数字检查
     * @param {Number||String} str 
     */
    static isStringNull(str) {
        return str == null || str.length === 0 || str === '' || str === undefined;
    }

    /**
     * 字符串省略
     * @param {String} str 字符串
     * @param {Number} limit 限定长度
     */
    static stringSlice(str,limit) {
        return str.length > limit ? `${str.slice(0,limit)}...` : str;
    }

    /**
     * @param {Number || String} str 手机号码
     */
    static phoneCheck(str) {
        const reg = /^[1][3|4|5|6|7|8|9][0-9]{9}$/;
        return reg.test(str);
    }
  
    /**
     * 
     * @param {Number || String} time 时间
     * @param {String} format
     * 'YYYY-MM-DD'  
     * 'YYYY-MM-DD, HH:mm:ss'
     * YYYY年MM月DD日
     * 'X' 毫秒时间戳
     */
    static timeFormat(time,format = 'YYYY-MM-DD HH:mm:ss') {
        return Moment(time).format(format);
    }
    
    /**
     * 转换毫秒级时间戳
     * @param {Number || String} time
     */
    static msec(time) {
        return Moment(time).valueOf();
    }

    /**
     * 计算年龄
     * @param time 出生日期
     * @return {string|number}
     */
    static ageComputed(time) {
        if (this.isStringNull(time)) {
            return '暂无';
        }
        return Moment().diff(this.timeFormat(time, 'YYYY-MM-DD'), 'years');
    }
    
    /**
     * 获取秒级时间戳
     */
    static second() {
        return Moment().unix();
    }

    /**
     * MD5加密
     * @param {String||Number} key 
     */
    static encryption(key) {
        return cryptoJs.MD5(key);
    }
    
    /**
     * 随机数 
     * @param {Number} l 随机数长度
     */
    static rndHexString (l) {
        let chars = 'ABCDEF1234567890';
        let max = chars.length;
        let str = '';
        for (let i = 0; i < l; i++) {
            str += chars.charAt(Math.floor(Math.random() * max));
        }
        return str;
    }

    /**
     * ASE加密
     * @param {String} text 加密内容
     * @param {String} textKey 加密KEY
     * @param {String} textIv 加密方式
     */
    static encryptCBC (code, key, iv)  {
        let _code = cryptoJs.enc.Utf8.parse(code),
            _key = cryptoJs.enc.Utf8.parse(key),
            _iv = cryptoJs.enc.Utf8.parse(iv);
        let encrypted = cryptoJs.AES.encrypt(_code, _key, {
            iv: _iv,
            mode: cryptoJs.mode.CBC,
            padding: cryptoJs.pad.Pkcs7
        });
        return encrypted.toString();
    }
    
    /**
     * AES解密
     * @param {*} word
     * @returns
     */
    static decrypt(word, key, iv) {
        let decrypt = cryptoJs.AES.decrypt(srcs, key, { iv: iv, mode: cryptoJs.mode.CBC, padding: cryptoJs.pad.Pkcs7 })
        let decryptedStr = decrypt.toString(cryptoJs.enc.Utf8);
        return decryptedStr.toString();
    }
    
    /**
    * AES加密
    * @param {*} word
    * @returns
    */
    static encrypt(word, key, iv) {
        let _key = cryptoJs.enc.Utf8.parse(key);
        let _iv = cryptoJs.enc.Utf8.parse(iv);
        let srcs = cryptoJs.enc.Utf8.parse(word);
        let encrypted = cryptoJs.AES.encrypt(srcs, _key, { iv: _iv, mode: cryptoJs.mode.CBC, padding: cryptoJs.pad.Pkcs7 })
        return encrypted.toString();
    }
}

export default Utils;