import Vue from "vue";
import Vuex from "vuex";
import {axiosApi} from '@utils/http.js';
import {Message} from 'element-ui';
import {mappingRouter} from '@config/asyncRouters';
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        //HeaderTitle
        HEADER_TITLE: "",
        //currentRouter
        CURRENT_ROUTER: "",
        //pageHeight
        PAGE_HEIGHT: "",
        //侧边栏导航配置
        NAV_CONFIG: [],
        //权限列表
        PERMISSIONS: [],
        //会员类型
        VIP_TYPE: [
            {
                label: '月度会员',
                id: 0,
                color: "text-main-32"
            },
            {
                label: '季度会员',
                id: 1,
                color: "text-main-33"
            },
            {
                label: '年度会员',
                id: 2,
                color: "text-main-34"
            }
        ],
        //订单状态
        ORDER_OPTIONS: [
            {
                label: '全部',
                id: ''
            },
            {
                label: '失败',
                id: -3,
                color: 'text-main-17'
            },
            {
                label: '过期',
                id: -2,
                color: 'text-main-24'
            },
            {
                label: '超时未支付',
                id: -1,
                color: 'text-main-26'
            },
            {
                label: '待支付',
                id: 0,
                color: 'text-main-6'
            },
            {
                label: '已支付',
                id: 1,
                color: 'text-main-17'
            }
        ],
        //支付类型
        PAY_TYPE: {
            "1": {
                text: '微信支付',
                color: 'text-main-17'
            },
            "2": {
                text: '支付宝',
                color: 'text-main-16'
            },
            "3": {
                text: '网银',
                color: 'text-main-15'
            },
            "4": {
                text: '余额',
                color: 'text-main-26'
            }
        },
        //视频状态
        VIDEO_OPTIONS: [
            {
                id: 'all',
                label: '全部',
                color: 'text-evaluate-active',
                colorValue: '#f9d526',
                updataLabel: "最后修改时间"
            },
            {
                id: '0',
                label: '待审核',
                color: 'text-evaluate-active',
                colorValue: '#f9d526',
                updataLabel: "最后修改时间"
            },
            {
                id: '1',
                label: '上线',
                color: 'text-main-17',
                colorValue: '#276eff',
                updataLabel: "上线时间"
            },
            {
                id: '2',
                label: '删除',
                color: 'text-main-17',
                colorValue: '#276eff',
                updataLabel: "删除时间"
            },
            {
                id: '4',
                label: '草稿',
                color: 'text-main-17',
                colorValue: '#276eff',
                updataLabel: "最后修改时间"
            },
            {
                id: '5',
                label: '下线',
                color: 'text-hint-1',
                colorValue: '#ff3434',
                updataLabel: "下线时间"
            },
            {
                id: '6',
                label: '驳回',
                color: 'text-warning-1',
                colorValue: '#f76923',
                updataLabel: "驳回时间"
            }
        ],
        //视频/动态 可修改状态
        CAN_STATUS_OPTIONS: {
            "0": [1, 6],
            "1": [5],
            "5": [1],
            "6": [1]
        },
        //用户角色配置
        ROLES: [
            {
                id: 1,
                label: '管理员'
            },
            {
                id: 3,
                label: '普通管理员'
            }
        ],
        //上传类型
        UPLOADED_TYPE: [
            {
                id: '-1',
                label: '全部'
            },
            {
                id: 0,
                label: '用户'
            },
            {
                id: 1,
                label: '后台'
            }
        ],
        TOKEN: localStorage.TOKEN || '',
        USER_ID: localStorage.USER_ID || '',
        USER_INFO: localStorage.USER_INFO ? JSON.parse(localStorage.USER_INFO) : '',
        QINIU_TOKEN: localStorage.QINIU_TOKEN || '',
        QINIU_URL: localStorage.QINIU_URL || '',
        QINIU_SD: localStorage.QINIU_SD || '',
        UPLOAD_SERVER_TYPE: localStorage.UPLOAD_SERVER_TYPE || '',
        //通知更新路由
        INFORM_ROUTER: false
    },
    getters: {
        getHeaderTitle: state => state.HEADER_TITLE,
        getCurrentRouter: state => state.CURRENT_ROUTER,
        getPageHeight: state => state.PAGE_HEIGHT,
        getSystemGui: () => ['/Login', '/Home', '*'],
        getUserPermissions: state => state.PERMISSIONS,
        getNavConfig: state => state.NAV_CONFIG,
        getUserInfo: state => state.USER_INFO,
        getToken: state => state.TOKEN,
        getUserId: state => state.USER_INFO.USER_ID,
        getQiniuToken: state => state.QINIU_TOKEN,
        getQiniuUrl: state => state.QINIU_URL,
        getQiniuSd: state => state.QINIU_SD,
        getUploadServerType: state => state.UPLOAD_SERVER_TYPE,
        getOrderById: state => id => state.ORDER_OPTIONS.find(order => order.id === id),
        getVipTypeById: state => id => state.VIP_TYPE.find(vip => vip.id == id),
        getVideoTypeById: state => id => state.VIDEO_OPTIONS.find(status => status.id == id),
        getPayTypeByKey: state => key => state.PAY_TYPE[key],
        getCanChangeShowOptions: state => state.VIDEO_OPTIONS.filter(status => !['2', '4'].includes(status.id)),
        getCanStatusByKey: (state, getters) => key => getters.getCanChangeShowOptions
            .filter(status => state.CAN_STATUS_OPTIONS[`${key}`].includes(parseInt(status.id))),
        getTabsByComponentsName: state => componentsName => {
            console.log(state.NAV_CONFIG, "更新后*----");
            if (Array.isArray(state.NAV_CONFIG) && state.NAV_CONFIG.length > 0) {
                // console.log(state.NAV_CONFIG, "导航*----------");
                //筛选出tab子页
                let tabs = state.NAV_CONFIG.find(nav => nav.name === componentsName).meta.tabs;
                //根据权限做二次筛选
                let check = state.PERMISSIONS
                    .filter(config => tabs.map(tab => tab.permission).includes(config.id) && config.toggle === 1)
                    .map(config => config.id);
                console.log(tabs.filter(tab => check.includes(tab.permission)), "导航首页**------");
                return tabs.filter(tab => check.includes(tab.permission));
            }
            return [];
        },
        getPageControlPermissions: state => (parentId, operationId) => {
            if (state.PERMISSIONS.length === 0
                || state.PERMISSIONS === []
                || state.PERMISSIONS === undefined) {
                return false;
            } else {
                let controls = state.PERMISSIONS.find(each => parentId === each.id);
                return controls.permissions.find(each => operationId === each.id).toggle === 1;
            }
        },
        getInformRouter: state => state.INFORM_ROUTER
    },
    mutations: {
        setHeaderTitle: (state, data) => Vue.set(state, 'HEADER_TITLE', data),
        setCurrentRouter: (state, data) => Vue.set(state, 'CURRENT_ROUTER', data),
        setPageHeight: (state, data) => Vue.set(state, 'PAGE_HEIGHT', data),
        setUserInfo: (state, data) => {
            try {
                localStorage.USER_INFO = JSON.stringify(data);
                Vue.set(state, 'USER_INFO', data);
            } catch (e) {
                console.log(e, 'setUserInfo_Vuex');
            }
        },
        setToken: (state, token) => {
            try {
                localStorage.TOKEN = token;
                Vue.set(state, 'TOKEN', localStorage.TOKEN);
            } catch (e) {
                console.log(e, 'setToken_Vuex');
            }
        },
        setUserId: (state, id) => {
            try {
                localStorage.USER_ID = id;
                Vue.set(state, 'USER_ID', localStorage.USER_ID);
            } catch (e) {
                console.log(e, 'setUserId')
            }
        },
        setQiniuToken: (state, token) => {
            try {
                localStorage.QINIU_TOKEN = token;
                Vue.set(state, 'QINIU_TOKEN', localStorage.QINIU_TOKEN);
            } catch (e) {
                console.log(e, 'setQiniuToken')
            }
        },
        setQiniuUrl: (state, url) => {
            try {
                localStorage.QINIU_URL = url;
                Vue.set(state, 'QINIU_URL', localStorage.QINIU_URL);
            } catch (e) {
                console.log(e, 'setQiniuUrl')
            }
        },
        setQiniuSd: (state, url) => {
            try {
                localStorage.QINIU_SD = url;
                Vue.set(state, 'QINIU_SD', localStorage.QINIU_SD);
            } catch (e) {
                console.log(e, 'setQiniuSd')
            }
        },
        setUploadServerType: (state, type) => {
            try {
                localStorage.UPLOAD_SERVER_TYPE = type;
                Vue.set(state, 'UPLOAD_SERVER_TYPE', localStorage.UPLOAD_SERVER_TYPE);
            } catch (e) {
                console.log(e, 'setUploadServerType')
            }
        },
        setPermissions: (state, permissions) => Vue.set(state, 'PERMISSIONS', permissions),
        setNavConfig: (state, router) => Vue.set(state, 'NAV_CONFIG', router),
        resetUser: state => {
            Object.keys(state).forEach(key => {
                let unCheck = [
                    "VIP_LEVEL",
                    "VIP_TYPE",
                    "PAY_TYPE",
                    "ORDER_OPTIONS",
                    "VIDEO_OPTIONS",
                    "SYSTEM_GUI",
                    "ROLES",
                    "UPLOADED_TYPE",
                    "QINIU_TOKEN",
                    "QINIU_URL",
                    "QINIU_SD",
                    "CAN_STATUS_OPTIONS",
                    "PAGE_HEIGHT"
                ];
                if (!unCheck.includes(key)) {
                    state[key] = Array.isArray(state[key]) ? [] : "";
                    localStorage.removeItem(key);
                }

            })
        },
        setInformRouter: (state, key = true) => Vue.set(state, 'INFORM_ROUTER', key)
    },
    actions: {
        changeUserInfo: (state, data) => state.commit('setUserInfo', data),
        changeToken: (state, token) => state.commit('setToken', token),
        changeUserId: (state, id) => state.commit('setUserId', id),
        changeQiniuServer: ({commit}) => {
            return new Promise(async (resolve, reject) => {
                let res = await axiosApi({
                    method: '/qiniutoken',
                    way: 'GET'
                });
                if (res.data.msg === 'success') {
                    commit('setQiniuToken', res.data.data.token);
                    commit('setQiniuUrl', res.data.data.server_url);
                    commit('setQiniuSd', res.data.data.visit_url);
                    commit('setUploadServerType', res.data.data.type);
                    resolve(res);
                } else {
                    Message.error("获取七牛token失败!" + res.data.msg);
                    reject(res);
                }
            });
        },
        /**
         * 根据权限生产路由表
         * @param dispatch
         * @param commit
         * @param getters
         * @return {Promise<unknown>}
         */
        generateNavConfig: ({dispatch, commit, getters}) => {
            return new Promise(async (resolve, reject) => {
                let res = await dispatch('getPermissionsById', getters.getUserInfo.id);
                if (res.data.msg === "success") {
                    if (Array.isArray(res.data.data)) {
                        //菜单级权限筛选
                        commit('setPermissions', res.data.data);
                        //路由配置
                        let router = mappingRouter(res.data.data);
                        if (Array.isArray(router) && router.length > 0) {
                            if (router[0].hasOwnProperty("path")) {
                                router[0].path = "/";
                                router.forEach((item, index) => {
                                    if (index !== 0 && item.path === "/") {
                                        item.path = `/${item.name}`;
                                    }
                                });
                            }
                        }
                        commit('setNavConfig', router);
                        resolve(router);
                    }
                } else {
                    reject(null);
                    Message.error(`获取权限失败-${res.data.msg}`);
                }
            });
        },
        /**
         *  查询用户权限表
         *  @param { String } uid 用户id
         *  @return promise
         */
        getPermissionsById: ({commit}, uid) => {
            return axiosApi({
                way: 'GET',
                method: '/mis/permissions',
                content: {uid}
            }, false);
        }
    },
    modules: {}
});
