export const uncheckRouters = [
    {
        path: "/Login",
        name: "Login",
        meta: {
            title: '用户登录',
            permission: "",
            requireAuth: false,
            icon: '',
            tabs: []
        },
        component: () => import(`@views/User/Login.vue`)
    }
];

export const asyncRouters = [
    {
        path: "/AdministerVideoList",
        name: "AdministerVideoList",
        meta: {
            title: '视频管理',
            permission: '104',
            requireAuth: true,
            icon: 'iconshipinguanli1',
            tabs: []
        },
        component: () =>
            import("@views/AdministerVideo/AdministerVideo-video-list.vue")
    },
    {
        path: "/AdministerCommunityList",
        name: "AdministerCommunityList",
        meta: {
            title: '社区管理',
            permission: '113',
            requireAuth: true,
            icon: 'iconshequ',
            tabs: []
        },
        component: () =>
            import("@views/AdministerCommunity/AdministerCommunity-list.vue")
    },
    {
        path: "/AdministerPay",
        name: "AdministerPay",
        meta: {
            title: '充值管理',
            permission: "",
            requireAuth: true,
            icon: 'iconchongzhi',
            tabs: [
                {
                    path: "AdministerPay-pay-List",
                    label: "金币充值列表",
                    permission: '106'
                },
                {
                    path: "AdministerPay-vip-list",
                    label: "VIP购买列表",
                    permission: '107'
                }
            ]
        },
        component: () =>
            import("@views/AdministerPay/index.vue")
    },
    {
        path: "/AdministerPaySetting",
        name: "AdministerPaySetting",
        meta: {
            title: '充值设置管理',
            permission: '103',
            requireAuth: true,
            icon: 'iconchongzhiguanli1',
            tabs: []
        },
        component: () =>
            import("@views/AdministerPaySetting/AdministerPaySetting-setting-list.vue")
    },
    //
    {
        path: "/AdministerVipList",
        name: "AdministerVipList",
        meta: {
            title: 'VIP管理',
            permission: '102',
            requireAuth: true,
            icon: 'iconvip-copy',
            tabs: []
        },
        component: () =>
            import("@views/AdministerVip/AdministerVip-vip-list.vue")
    },
    {
        path: "/AdministerBankList",
        name: "AdministerBankList",
        meta: {
            title: '银行管理',
            permission: '105',
            requireAuth: true,
            icon: 'iconyinxing1',
            tabs: []
        },
        component: () =>
            import("@views/AdministerBank/AdministerBank-bank-list.vue")
    },
    {
        path: "/AdministerUserList",
        name: "AdministerUserList",
        meta: {
            title: '用户管理',
            permission: '110',
            requireAuth: true,
            icon: 'iconyonghuguanli1',
            tabs: []
        },
        component: () =>
            import("@views/AdministerUser/AdministerUser-user-list.vue")
    },
    {
        path: "/AdministerFinance",
        name: "AdministerFinance",
        meta: {
            title: '财务管理',
            permission: "",
            requireAuth: true,
            icon: 'iconcaiwuguanli',
            tabs: [
                {
                    path: "AdministerFinance-apply-list",
                    label: "提现申请",
                    permission: '109'
                },
                {
                    path: "AdministerFinance-daily",
                    label: "日报",
                    permission: '111'
                },
                {
                    path: "AdministerFinance-monthly",
                    label: "月报",
                    permission: '112'
                },
                {
                    path: "AdministerFinance-detail",
                    label: "资金明细",
                    permission: '118'
                }
            ]
        },
        component: () =>
            import("@views/AdministerFinance/index.vue")
    },
    {
        path: "/AdministerVersion",
        name: "AdministerVersion",
        meta: {
            title: '版本管理',
            permission: '115',
            requireAuth: true,
            icon: 'iconbanbenguanli',
            tabs: []
        },
        component: () =>
            import("@views/AdministerVersion/AdministerVersion-index.vue")
    },
    {
        path: "/AdministerSystem",
        name: "AdministerSystem",
        meta: {
            title: '系统',
            permission: "",
            requireAuth: true,
            icon: 'iconsystem',
            tabs: [
                {
                    path: "AdministerSystem-user-list",
                    label: "账号管理",
                    permission: '101'
                },
                {
                    path: "AdministerSystem-Sms",
                    label: "短信管理",
                    permission: '116'
                },
                {
                    path: "AdministerSystem-param-setting",
                    label: "参数设置",
                    permission: '114'
                }
            ]
        },
        component: () =>
            import("@views/AdministerSystem/index.vue")
    },
    {
        path: "*",
        name: "404",
        meta: {
            title: '404',
            permission: "",
            requireAuth: false,
            icon: '',
            tabs: []
        },
        component: () => import("@views/404/index.vue")
    }
];

/**
 * 根据权限匹配出符合的路由
 * @param { Array } roles 权限表
 * @return { Array } Router 返回匹配的路由结构
 */
export function mappingRouter(roles) {
    let havePermissions = roles
           .filter(role => role.toggle === 1)
              .map(role => role.id);
    // 充值管理页面
    const PAY_PAGES = ['106', '107'];
    // 财务管理页面
    const FINNANCE = ['109', '111', '112', '118'];
    //系统配置页面
    const SYSTEM = ['101', '116', '114'];

    let routers = [];
    asyncRouters.forEach(config => {
        if (havePermissions.includes(config.meta.permission)
            || config.meta.title === "系统"
            || config.name === '404') {
            routers.push(config);
        }  else if (config.meta.title === "充值管理" && PAY_PAGES.some(id => havePermissions.includes(id))) {
            routers.push(config);
        } else if (config.meta.title === "财务管理" && FINNANCE.some(id => havePermissions.includes(id))) {
            routers.push(config);
        }
    });
    return routers;
};


