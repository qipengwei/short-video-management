
const PROJECT_CONFIG = {
    "shooter-dev": {
        //URL
        URL: 'http://shooterapicloud.204206.pw:7000',
        // 文件名
        FILE_NAME: 'shooter',
        //打包文件夹
        DIR: 'dev',
        //路由是否使用history模式
        HISTORY: false,
        // 项目名称
        PROJECT_NAME: '小苹果'
    },
    "shooter-prod": {
        //URL
        URL: 'http://shooterapicloud.204206.pw:7000',
        // 文件名
        FILE_NAME: 'shooter',
        //打包文件夹
        DIR: 'prod',
        //路由是否使用history模式
        HISTORY: true,
        // 项目名称
        PROJECT_NAME: '小苹果'
    }
};

module.exports = PROJECT_CONFIG;



 