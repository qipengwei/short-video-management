import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

//浏览器滚动时间优化
import 'default-passive-events';
//animate
import '@assets/animation/animate.css';
//阿里矢量图
import '@assets/icon/iconfont.css';
// 全局css
import '@assets/less/main.less';
// EventBus
const bus = new Vue();
Vue.prototype.$EventBus = bus;
// 请求
import { axiosApi } from '@utils/http.js';
Vue.prototype.$axiosApi = axiosApi;
//工具类
import Utils from '@utils/index.js';
Vue.prototype.$Utils = Utils;
// Element
import 'element-ui/lib/theme-chalk/index.css';
import element from 'element-ui';

//业务组件 全局挂载
import Tabs from '@components/Tabs';
import Btn from '@components/Btn';
import Container from "@components/Container";
import NoData from '@components/NoData';
import NotFound from '@views/404/index';

Vue.component(NoData.name, NoData);
Vue.component(Tabs.name, Tabs);
Vue.component(Btn.name, Btn);
Vue.component(Container.name, Container);
Vue.component(NotFound.name, NotFound);

//wow.js
import wow from 'wowjs';
Vue.prototype.$wow = wow;

// 查询用户列表分页指令
Vue.directive('loadmore', {
    bind (el, binding) {
        // 获取element-ui定义好的scroll盒子
        const SELECTWRAP_DOM = el.querySelector('.el-select-dropdown .el-select-dropdown__wrap');
        SELECTWRAP_DOM.addEventListener('scroll', function () {
        const CONDITION = this.scrollHeight - this.scrollTop <= this.clientHeight;
        if (CONDITION) {
            binding.value()
        }
      })
    }

});

Vue.use(element);
Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");
